function matchesPlayedPerYear() {
    //src/public/
    fetch("./output/matchesPlayedPerYear.json")
        .then(response => response.json())
        .then((data) => {
            // console.log(data);
            let newData=[];
            for(let index in data){
                newData.push({name:index,y:data[index]});
            }
            // console.log(newData);
           // Create the chart
           Highcharts.chart('container1', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Match Played Per Year'
            },
            subtitle: {
                text: 'Source: <a href="http://en.wikipedia.org/wiki/List_of_cities_proper_by_population%22%3EWikipedia</a>'
            },
            xAxis: {
                type: 'category',
                labels: {
                    rotation: -45,
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Population (millions)'
                }
            },
            legend: {
                enabled: false
            },
            tooltip: {
                pointFormat: 'Population in 2017: <b>{point.y:.1f} millions</b>'
            },
            series: [{
                name: 'Population',
                data: newData,
                dataLabels: {
                    enabled: true,
                    rotation: -90,
                    color: '#FFFFFF',
                    align: 'right',
                    format: '{point.y:.1f}', // one decimal
                    y: 10, // 10 pixels down from the top
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            }]
        });
        })
}
matchesPlayedPerYear();

function matchesWonPerTeam() {
    fetch("./output/matchesWonPerTeam.json")
        .then(response => response.json())
        .then((data) => {
            let keys = Object.keys(data);
            let yearsArray = ["2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017"];

            let dataArray = [];
            for (let index = 0; index < yearsArray.length; index++) {
                let indivisualYearData = [];
                for (const key in data) {
                    let flag = false;
                    let entries = Object.entries(data[key]);
                    for (let innerIndex = 0; innerIndex < entries.length; innerIndex++) {
                        if (entries[innerIndex][0] === yearsArray[index]) {
                            indivisualYearData.push(entries[innerIndex][1]);
                            flag = true;
                        }
                    }
                    if (flag == false) {
                        indivisualYearData.push(0);
                    }
                }
                dataArray.push({ name: yearsArray[index], data: indivisualYearData });
            }

            Highcharts.chart('container2', {
                chart: {
                    type: 'bar'
                },
                title: {
                    text: 'MATCHES WON PER TEAM PER SEASON',
                    fontFamily: 'sans-serif'
                },
                subtitle: {
                    text: ''
                },
                xAxis: {
                    categories: keys,
                    title: {
                        text: null
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: '',
                        align: 'high'
                    },
                    labels: {
                        overflow: 'justify'
                    }
                },
                tooltip: {
                    valueSuffix: ''
                },
                plotOptions: {
                    bar: {
                        dataLabels: {
                            enabled: true
                        }
                    }
                },
                legend: {
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'top',
                    x: -40,
                    y: 80,
                    floating: true,
                    borderWidth: 5,
                    backgroundColor:
                        Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
                    shadow: true
                },
                credits: {
                    enabled: false
                },
                series: dataArray
            });
        })

}

matchesWonPerTeam();




function extraRunsConcededIn2016(){
    fetch('./output/extraRunsConcededIn2016.json')
  .then((response) => response.json())
  .then((iplData) =>{
    let newData=[];
    // console.log(iplData);
            for(let index in iplData){
                newData.push({name:index, y:iplData[index]});
            }
            Highcharts.chart('container3', {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                title: {
                    text: 'Browser market shares in January, 2018'
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                accessibility: {
                    point: {
                        valueSuffix: '%'
                    }
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                        }
                    }
                },
                series: [{
                    name: 'Brands',
                    colorByPoint: true,
                    data: newData
                }]
            });
  });
}
extraRunsConcededIn2016();

function topEconomicalBowler2015() {
    fetch("./output/topEconomicalBowler2015.json")
        .then((response) => response.json())
        .then((data) => {
            let bowlerName = [];
            let economy = [];
            for (let i = 0; i < data.length; i++) {
                bowlerName.push(data[i][0]);
                economy.push(Number.parseFloat(data[i][1]));
            }
            const chart = Highcharts.chart('container4', {
                title: {
                    text: 'TOP ECONOMICAL RATED BOWLER IN 2015',
                    fontFamily: 'sans-serif'
                },
                subtitle: {
                    text: ''
                },
                xAxis: {
                    categories: bowlerName
                },
                series: [{
                    type: 'column',
                    colorByPoint: true,
                    data: economy,
                    showInLegend: false
                }]
            });
            
        })
}
topEconomicalBowler2015();

function wonMatchAndToss() {
    fetch("./output/wonMatchAndToss.json")
        .then((response) => response.json())
        .then((data) => {
            let arrayData = [];
            console.log(data);
            for (const key in data) {
                arrayData.push({ name: key, y: data[key] })
            }
            console.log(arrayData);
            Highcharts.chart('container5', {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                title: {
                    text: 'TEAM THAT WON MATCHES AND TOSS',
                    fontFamily: 'sans-serif'
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.0f}</b>'
                },
                accessibility: {
                    point: {
                        valueSuffix: ''
                    }
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: false
                        },
                        showInLegend: true
                    }
                },
                series: [{
                    name: '',
                    colorByPoint: true,
                    data: arrayData
                }]
            });
        })
}

wonMatchAndToss();


function mostPlayerOfMatchAwardsPerSeason() {
    fetch("./output/playersWithHighestNumberOfPlayerOfTheMatchAwards.json")
        .then(response => response.json())
        .then((data) => {

            let dataArray = [];

            for (const key in data) {
                let entries = Object.entries(data[key]);
                let blankElement = Number.parseInt(key) - 2008;
                let value = 0;
                let mergeArray = [];
                while (value < blankElement) {
                    mergeArray.push('');
                    value++;
                }
                for (let index = 0; index < entries.length; index++) {
                    mergeArray.push(entries[index][1]);
                    dataArray.push({ name: entries[index][0], data: mergeArray })
                    mergeArray = mergeArray.slice(0, mergeArray.length - 1);
                }

            }

            Highcharts.chart('container6', {
                chart: {
                    type: 'spline'
                },

                legend: {
                    symbolWidth: 40.
                },

                title: {
                    text: 'MOST PLAYER OF MATCH PER SEASON',
                    fontFamily: 'sans-serif'
                },

                subtitle: {
                    text: ''
                },

                yAxis: {
                    title: {
                        text: 'Award Numbers'
                    },
                    accessibility: {
                        description: 'Award Numbers'
                    }
                },

                xAxis: {
                    title: {
                        text: 'Years'
                    },
                    accessibility: {
                        description: ''
                    },
                    categories: ['2008', '2009', '2010', '2011', '2012', '2013', '2014', '2015', '2016', '2017']
                },

                tooltip: {
                    valueSuffix: ''
                },

                plotOptions: {
                    series: {
                        point: {
                            events: {
                                click: function () {
                                    window.location.href = this.series.options.website;
                                }
                            }
                        },
                        cursor: 'pointer'
                    }
                },

                series: dataArray,

                responsive: {
                    rules: [{
                        condition: {
                            maxWidth: 550
                        },
                        chartOptions: {
                            chart: {
                                spacingLeft: 3,
                                spacingRight: 3
                            },
                            legend: {
                                itemWidth: 150
                            },
                            xAxis: {
                                categories: ['2008', '2009', '2010', '2011', '2012', '2013', '2014', '2015', '2016', '2017'],
                                title: ''
                            },
                            yAxis: {
                                visible: false
                            }
                        }
                    }]
                }
            });

        })


}

mostPlayerOfMatchAwardsPerSeason();

function strikeRate() {

    fetch('./output/strikeRateOfBatsmanForEachSeason.json')
        .then((data) => {
            return data.json();
        }).then((records) => {
            const { IPLseason, STRIKE_RATE } = objectToArray(records);
            lineChart(IPLseason, STRIKE_RATE);
        })
        .catch(err => {
            console.error(err);
        })

    function objectToArray(records) {
        const IPLseason = [];
        const STRIKE_RATE = []
        for (let season of records) {
            IPLseason.push(season.season);
            STRIKE_RATE.push(season.strikeRate);
        }
        return { IPLseason, STRIKE_RATE };
    }

    function lineChart(IPLseason, STRIKE_RATE) {
        Highcharts.theme = {
            colors: ['#12b556']
        }
        Highcharts.setOptions(Highcharts.theme);
        Highcharts.chart('container7', {

            title: {
                text: 'StrikeRate of DHONI in IPL'
            },

            subtitle: {
                text: 'Source: Kaggle.com'
            },

            yAxis: {
                title: {
                    text: 'StrikeRate'
                }
            },

            xAxis: {
                accessibility: {
                    rangeDescription: IPLseason
                }
            },

            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle'
            },

            plotOptions: {
                series: {
                    label: {
                        connectorAllowed: false
                    },
                    pointStart: 2010
                }
            },

            series: [{
                name: 'StrikeRate',
                data: STRIKE_RATE
            }],

            responsive: {
                rules: [{
                    condition: {
                        maxWidth: 500
                    },
                    chartOptions: {
                        legend: {
                            layout: 'horizontal',
                            align: 'center',
                            verticalAlign: 'bottom'
                        }
                    }
                }]
            }

        });
    }
}

strikeRate();

function dismissed() {
    fetch('./output/highestTimePlayerDismissed.json')
        .then((data) => {
            return data.json();
        }).then((records) => {
            const data = objectToArray(records);
            barChart(data);
        })
        .catch(err => {
            console.error(err);
        })
    function objectToArray(records) {

        const formatData = [];
        for (let player in records) {
            formatData.push([`${records[player].batsman} vs ${records[player].bowler}`, records[player].count])
        }
        return formatData;
    }


    function barChart(records) {
        Highcharts.theme = {
            colors: ['#e8886b']
        }
        Highcharts.setOptions(Highcharts.theme);
        Highcharts.chart('container8', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Number of times one player has been dismissed by another player'
            },
            subtitle: {
                text: 'Source: <a href="https://www.kaggle.com/manasgarg/ipl">kaggle</a>'
            },
            xAxis: {
                type: 'category',
                labels: {
                    rotation: -40,
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Number of time dismissed'
                }
            },
            legend: {
                enabled: false
            },
            tooltip: {
                pointFormat: 'Most Dismissed Player: <b>{point.y:.1f} </b>'
            },
            series: [{
                name: 'Population',
                data: records,
                dataLabels: {
                    enabled: true,
                    rotation: -90,
                    color: '#FFFFFF',
                    align: 'right',
                    format: '{point.y:.1f}', // one decimal
                    y: 10, // 10 pixels down from the top
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            }]
        });
    }
}

dismissed();











