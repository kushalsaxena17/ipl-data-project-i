const fs = require('fs');
exports.matchesWonPerTeam = (matchesData) => {
    let winners = {};
    for (let index = 0; index < matchesData.length; index++) {
        if (matchesData[index].winner) {
            if (winners[matchesData[index]['winner']]) {
                if (winners[matchesData[index]['winner']][matchesData[index]['season']]) {
                    winners[matchesData[index]['winner']][matchesData[index]['season']] += 1;
                } else {
                    winners[matchesData[index]['winner']][matchesData[index]['season']] = 1;
                }
            } else {
                winners[matchesData[index]['winner']] = {};
                winners[matchesData[index]['winner']][matchesData[index]['season']] = 1;

            }
        }
    }

    fs.writeFile('/home/kushal/Desktop/ipl-data-Project-1 (copy)/src/public/output/matchesWonPerTeam.json', JSON.stringify(winners, null, 4), 'utf-8', (err) => {
        if (err) {
            console.log(err);
        }
    })

};
