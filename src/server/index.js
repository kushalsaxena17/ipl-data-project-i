const matches = '/home/kushal/Desktop/ipl-data-Project-1 (copy)/src/data/matches.csv';
const deliveries = '/home/kushal/Desktop/ipl-data-Project-1 (copy)/src/data/deliveries.csv';

const csv = require('csvtojson')

const { matchesWonPerTeam } = require("/home/kushal/Desktop/ipl-data-Project-1 (copy)/src/server/matchesWonPerTeam.js")
const { matchesPlayedPerYear } = require("/home/kushal/Desktop/ipl-data-Project-1 (copy)/src/server/matchesPlayedPerYear.js")
const { extraRunsConcededIn2016 } = require("/home/kushal/Desktop/ipl-data-Project-1 (copy)/src/server/extraRunsConcededIn2016.js")
const { topEconomicalBowler2015 } = require("/home/kushal/Desktop/ipl-data-Project-1 (copy)/src/server/topEconomicalBowler2015.js");
//const { teamWonTossAlsoWonMatchCount } = require("/home/kushal/Desktop/ipl-data-Project-1 (copy)/src/server/teamWonTossAlsoWonMatch.js")
const {playersWithHighestNumberOfPlayerOfTheMatchAwards}= require("/home/kushal/Desktop/ipl-data-Project-1 (copy)/src/server/playersWithHighestNumberOfPlayerOfTheMatchAwards.js");
const {strikeRateOfBatsmanForEachSeason}= require("/home/kushal/Desktop/ipl-data-Project-1 (copy)/src/server/strikeRateOfBatsmanForEachSeason.js")
const {highestTimePlayerDismissed}= require("/home/kushal/Desktop/ipl-data-Project-1 (copy)/src/server/hisghestNumTimesPlayerDismissedByAnotherPlayer.js")
const {bestEconomyInSuperOver}= require("/home/kushal/Desktop/ipl-data-Project-1 (copy)/src/server/bowlerWithBestEconomyInSuperOvers.js")
const {WonMatchAndToss} = require("/home/kushal/Desktop/ipl-data-Project-1 (copy)/src/server/teamWonTossAlsoWonMatch.js");


csv()
    .fromFile(matches)
    .then((jsonObj) => {

        let matchesData = jsonObj;

        csv()
            .fromFile(deliveries)
            .then((jsonObj) => {
                let deliveriesData = jsonObj;
                functionCall(matchesData, deliveriesData);
            })
    })



function functionCall(matchesData, deliveriesData) {

    matchesWonPerTeam(matchesData);
    matchesPlayedPerYear(matchesData);
    extraRunsConcededIn2016(matchesData, deliveriesData);
    topEconomicalBowler2015(matchesData, deliveriesData);
    playersWithHighestNumberOfPlayerOfTheMatchAwards(matchesData);
    strikeRateOfBatsmanForEachSeason(matchesData, deliveriesData);
  //  teamWonTossAlsoWonMatchCount(matchesData);
    highestTimePlayerDismissed(deliveriesData);
    bestEconomyInSuperOver(deliveriesData);
    WonMatchAndToss(matchesData);

}










