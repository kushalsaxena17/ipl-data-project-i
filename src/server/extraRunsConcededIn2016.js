const fs = require('fs');
exports.extraRunsConcededIn2016=(matchesData, deliveriesData)=>{
    let id = [];
    let extraRuns = {};
    for (let index = 0; index < matchesData.length; index++) {
        if (matchesData[index]['season'] == 2016) {
            id.push(matchesData[index].id);
        }
    }

    for (let index = 0; index < deliveriesData.length; index++) {
        if (id.indexOf(deliveriesData[index]['match_id']) != -1) {

            if (extraRuns[deliveriesData[index]['bowling_team']]) {
                extraRuns[deliveriesData[index]['bowling_team']] += Number.parseInt(deliveriesData[index]['extra_runs']);
            } else {
                extraRuns[deliveriesData[index]['bowling_team']] = Number.parseInt(deliveriesData[index]['extra_runs']);
            }


        }
    }

    fs.writeFile('/home/kushal/Desktop/ipl-data-Project-1 (copy)/src/public/output/extraRunsConcededIn2016.json', JSON.stringify(extraRuns, null, 4), 'utf-8', (err) => {
        if (err) {
            console.log(err);
        }
    })


};