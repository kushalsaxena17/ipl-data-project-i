const fs = require('fs');
exports.topEconomicalBowler2015=(matches, deliveries)=>{
    let id = [];
    let economy = {};
    for (let index = 0; index < matches.length; index++) {
        if (matches[index]['season'] == 2015) {
            id.push(matches[index].id);
        }
    }
    for (let index = 0; index < deliveries.length; index++) {
        if (id.indexOf(deliveries[index]['match_id']) != -1)
            if (economy[deliveries[index]['bowler']]) {
                economy[deliveries[index]['bowler']] += Number.parseInt(deliveries[index]['total_runs']);
            } else {
                economy[deliveries[index]['bowler']] = Number.parseInt(deliveries[index]['total_runs']);
            }
    }
    function oversOfBowler(bowlerName) {
        let overs = 0;
        for (let index = 0; index < deliveries.length; index++) {
            if (id.indexOf(deliveries[index]['match_id']) != -1) {
                if (deliveries[index]['bowler'] == bowlerName && deliveries[index]['ball'] == '1') {
                    overs += 1;
                }
            }
        }
        return overs;
    }
    for (const key in economy) {
        economy[key] = (economy[key] / oversOfBowler(key)).toFixed(2);
    }
    let economyArray = [];
    for (const key in economy) {
        economyArray.push([key, economy[key]]);
    }
    economyArray.sort((a, b) => {
        return a[1] - b[1];
    })
    fs.writeFile('../public/output/topEconomicalBowler2015.json', JSON.stringify(economyArray.slice(0, 10), null, 4), 'utf-8', (err) => {
        if (err) {
            console.log(err);
        }
    })
}