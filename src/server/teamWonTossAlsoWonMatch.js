const fs = require('fs');
exports.WonMatchAndToss=(matchesData)=>{
    const teamWonTossAndMatch = {};
    for (let index = 0; index < matchesData.length; index++) {
        if (matchesData[index]["toss_winner"] === matchesData[index]["winner"]) {
            if (!teamWonTossAndMatch[matchesData[index]["winner"]]) {
                teamWonTossAndMatch[matchesData[index]["winner"]] = 1
            }
            else {
                teamWonTossAndMatch[matchesData[index]["winner"]] += 1
            }
        }
        fs.writeFile('../public/output/wonMatchAndToss.json', JSON.stringify(teamWonTossAndMatch, null, 4), 'utf-8', (error) => {
            if (error) {
                console.log(error);
            }
        })
    }
}