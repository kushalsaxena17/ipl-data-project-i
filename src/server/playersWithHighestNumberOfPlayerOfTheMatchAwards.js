const fs = require('fs');

exports.playersWithHighestNumberOfPlayerOfTheMatchAwards=(IPLmatch)=>{
  const MAN_OF_THE_MATCH = {};
  const manOfMatch = {};
  IPLmatch.forEach(season);
  IPLmatch.reduce(manOfTheMatch);


  function season(element) {
      MAN_OF_THE_MATCH[element.season] = {};
      manOfMatch[element.season] = [];

  }
  function manOfTheMatch(tot, match) {

      MAN_OF_THE_MATCH[match.season][match.player_of_match] = MAN_OF_THE_MATCH[match.season][match.player_of_match] ?? 0;
      MAN_OF_THE_MATCH[match.season][match.player_of_match] += 1
      return MAN_OF_THE_MATCH;
  }

  for (let season in MAN_OF_THE_MATCH) {
      for (let player in MAN_OF_THE_MATCH[season])
          manOfMatch[season].push({ name: player, count: MAN_OF_THE_MATCH[season][player] });
  }
  const manOfMatchPerSeason = [];
  for (let season in manOfMatch) {
      manOfMatch[season].sort((a, b) => b.count - a.count)
      const manofSeason = manOfMatch[season].shift();
      manOfMatchPerSeason.push({ season: +season, manOfMatch: manofSeason.name })
  }

  fs.writeFile('/home/kushal/Desktop/ipl-data-Project-1 (copy)/src/public/output/playersWithHighestNumberOfPlayerOfTheMatchAwards.json', JSON.stringify(manOfMatchPerSeason, null, 4), 'utf-8', (err) => {
    if (err) {
        console.log(err);
    }
})
}






