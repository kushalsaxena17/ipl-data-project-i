const fs = require('fs');
exports.highestTimePlayerDismissed=(IPLrecords)=>{
    const dismiss = {};
    const dismissPlayer = [];
    IPLrecords.forEach(records => {
        if (records.player_dismissed != '') {
            dismiss[records.player_dismissed] = {}
        }
    })

    IPLrecords.reduce(countDismiss);

    function countDismiss(tot, records) {
        if (records.player_dismissed != '') {
            if( records.dismissal_kind != 'run out'){
                dismiss[records.player_dismissed][records.bowler] = dismiss[records.player_dismissed][records.bowler] ?? 0;
                dismiss[records.player_dismissed][records.bowler] += 1;
            }
                    
        }
        return dismiss;
    }

    for (let element in dismiss) {
        for (let dismissingBowler in dismiss[element]) {
            dismissPlayer.push({ batsman: element, bowler: dismissingBowler, count: dismiss[element][dismissingBowler] });
        }
    }
    
   
    dismissPlayer.sort((a, b) => { return b.count - a.count });
    const cnt = dismissPlayer.reduce(function (a, b) { return Math.max(a, b.count); }, 0);
    const dataChart=dismissPlayer.filter(elem => elem.count == cnt);
    

    fs.writeFile('/home/kushal/Desktop/ipl-data-Project-1 (copy)/src/public/output/highestTimePlayerDismissed.json', JSON.stringify(dataChart, null, 4), 'utf-8', (err) => {
        if (err) {
            console.log(err);
        }
    })
}


