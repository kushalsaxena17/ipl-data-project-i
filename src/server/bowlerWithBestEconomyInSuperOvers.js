const fs = require('fs');
exports.bestEconomyInSuperOver=(IPLrecords)=>{
    const bowler = IPLrecords.reduce(bowledSuperOver, {});
    const SUPER_OVER = objectToArray(bowler);
    SUPER_OVER.sort((a, b) => { return a.runs - b.runs });

    const cnt = SUPER_OVER.reduce(function (a, b) { return Math.min(a, b.runs); }, Infinity);
    const dataChart = SUPER_OVER.filter(elem => elem.runs == cnt);
    fs.writeFile('/home/kushal/Desktop/ipl-data-Project-1 (copy)/src/public/output/bestEconomyInSuperOver.json', JSON.stringify(dataChart, null, 4), 'utf-8', (err) => {
        if (err) {
            console.log(err);
        }
    })

}

function bowledSuperOver(bowler, records) {
    if (records.is_super_over != 0) {
        bowler[records.bowler] = bowler[records.bowler] ?? 0;
        bowler[records.bowler] += +records.total_runs;
    }
    return bowler;
}

function objectToArray(bowler) {
    const array = [];
    for (let ele in bowler) {
        array.push({ player: ele, runs: bowler[ele] });
    }
    return array;
}

