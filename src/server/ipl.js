const matchesPlayedPerYear = require("/home/kushal/Desktop/ipl-data-Project-1 (copy)/src/server/matchesPlayedPerYear.js");
const matchesWonPerTeam = require("/home/kushal/Desktop/ipl-data-Project-1 (copy)/src/server/matchesWonPerTeam.js");
const extraRunsConcededIn2016 = require("/home/kushal/Desktop/ipl-data-Project-1 (copy)/src/server/extraRunsConcededIn2016.js");
const topEconomicalBowler2015 = require("/home/kushal/Desktop/ipl-data-Project-1 (copy)/src/server/topEconomicalBowler2015.js");
 
const playersWithHighestNumberOfPlayerOfTheMatchAwards = require("/home/kushal/Desktop/ipl-data-Project-1 (copy)/src/server/playersWithHighestNumberOfPlayerOfTheMatchAwards.js")
const strikeRateOfBatsmanForEachSeason= require("/home/kushal/Desktop/ipl-data-Project-1 (copy)/src/server/strikeRateOfBatsmanForEachSeason.js")
const highestTimePlayerDismissed = require("/home/kushal/Desktop/ipl-data-Project-1 (copy)/src/server/hisghestNumTimesPlayerDismissedByAnotherPlayer.js")
const bestEconomyInSuperOver = require("/home/kushal/Desktop/ipl-data-Project-1 (copy)/src/server/bowlerWithBestEconomyInSuperOvers.js")
const WonMatchAndToss = require("/home/kushal/Desktop/ipl-data-Project-1 (copy)/src/server/teamWonTossAlsoWonMatch.js");

module.exports = {  matchesWonPerTeam, extraRunsConcededIn2016, matchesPlayedPerYear,
    topEconomicalBowler2015, playersWithHighestNumberOfPlayerOfTheMatchAwards, strikeRateOfBatsmanForEachSeason, highestTimePlayerDismissed, bestEconomyInSuperOver, WonMatchAndToss
}
